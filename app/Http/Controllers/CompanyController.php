<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\Company\CompanyCreateRequest;
use App\Http\Requests\Company\CompanyUpdateRequest;

class CompanyController extends Controller
{
    public function show($id)
    {
        $company = Company::find($id);

        if (!$company) {
            abort(404);
        }

        return view('company.edit', compact('company'));
    }

    public function create()
    {
        return view('company.create');
    }

    public function update(CompanyUpdateRequest $request, $id)
    {
        $company = Company::find($id);

        if (!$company) {
            abort(404);
        }

        $data = $request->only('name', 'description', 'address');

        $company->fill($data);

        if ($company->save()) {
            return ['status' => true];
        }
        return ['status' => false, 'message' => 'Company was not updated'];
    }

    public function destroy($id)
    {
        $company = Company::find($id);

        if (!$company) {
            return response()->json(['status' => false, 'message' => 'There no such company']);
        }

        if ($company->delete()) {
          return  response()->json(['status' => true]);
        }
        return response()->json(['status' => false, 'message' => 'Company was not deleted']);
    }

    public function store(CompanyCreateRequest $request)
    {
        $company = new Company();

        $data = $request->only('name', 'description', 'address');

        $company->fill($data);

        if ($company->save()) {
            return ['status' => true];
        }
        return ['status' => false, 'message' => 'Company was not saved'];
    }
}
