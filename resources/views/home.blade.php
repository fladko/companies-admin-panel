@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div>
                            Dashboard
                        </div>
                        <div>
                            <a href="company/create" type="button" class="btn btn-success">Add company</a>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if($companies)
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Company name</th>
                                    <th scope="col">Company description</th>
                                    <th scope="col">Company address</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($companies as $company)
                                <tr class="company-{!! $company['id'] !!}">
                                    <th scope="row">{!! $company['id'] !!}</th>
                                    <td>{!! $company['name'] !!}</td>
                                    <td>{!! $company['description'] !!}</td>
                                    <td>{!! $company['address'] !!}</td>
                                    <td>
                                        <div class="btn-group" role="group">
                                            <a type="button" href="/company/{!! $company['id'] !!}" class="btn btn-primary ">Edit</a>
                                            <button type="button" data-company_id="{!! $company['id'] !!}" class="btn btn-danger destroy">Delete</button>
                                        </div>
                                    </td>
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                                {!! $companies->render() !!}
                        @else
                            There no companies yet
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
