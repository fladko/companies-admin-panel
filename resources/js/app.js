/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');
//
// /**
//  * The following block of code may be used to automatically register your
//  * Vue components. It will recursively scan this directory for the Vue
//  * components and automatically register them with their "basename".
//  *
//  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
//  */
//
// // const files = require.context('./', true, /\.vue$/i)
// // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
//
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
//
// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */
//
// const app = new Vue({
//     el: '#app',
// });
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#edit-company').submit(async function(e) {
        e.preventDefault();

        var form = $(this);
        var url = form.attr('action');

        await editOrSaveCompany(url, form.serialize());
    });

    $('#create-company').submit(async function(e) {
        e.preventDefault();

        var form = $(this);
        var url = form.attr('action');

        await editOrSaveCompany(url, form.serialize());
    });

    $('.destroy').click(async function () {
        let id = $(this).data('company_id');
        let ajax = new Ajax();
        var result = await ajax.saveForm('/company/' + id, 'delete');

        if (result.status) {
            window.location.replace('/')
        } else {
            alert(result.message);
        }
    });
});

async function editOrSaveCompany(url, formData) {
    let ajax = new Ajax();
    var result = await ajax.saveForm(url, 'post', formData);

    if (result.status) {
        window.location.replace('/')
    } else {
        alert(result.message);
    }
}

var Ajax = class {
    async sendAjax(url, method, formData) {
        if (!url) {
            return Promise.reject(new Error('fail'))
        }
        return $.ajax({
            url: url,
            method: method,
            data: formData
        });
    };

    async saveForm(url, method, formData) {
        return await this.sendAjax(url, method, formData);
    };
};
